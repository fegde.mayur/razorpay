from django.contrib import admin
from payment.models import Transaction, Order
# Register your models here.


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = ['id', 'method']


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'amount', 'target', 'user']
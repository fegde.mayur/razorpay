from payment.models import Transaction, Order
from django.core.exceptions import ObjectDoesNotExist


def update_or_create_txn(pg_response):
    order_id = pg_response['order_id']

    try:
        o = Transaction.objects.get(order__id=order_id)
    except ObjectDoesNotExist:
        order: Order = Order.objects.get(rz_order_id=order_id)
        o = Transaction.objects.create(order=order)
    finally:
        o.order_info = pg_response['order_info']
        o.amount = pg_response['amount']
        o.txn_id = pg_response['txn_id']
        o.currency = pg_response['currency']
        o.method = pg_response['method']
        o.card_id = pg_response['card_id']
        o.bank = pg_response['bank']
        o.wallet = pg_response['wallet']
        o.vpa = pg_response['vpa']
        o.status = pg_response['status']
        o.save()



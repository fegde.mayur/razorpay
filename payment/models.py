from django.db import models
from pg_default.models import User, DateMixing


# Create your models here.


class Order(DateMixing):
    TARGET_CHOICES = (('1', 'Plan'), ('2', 'Data Download'))

    rz_order_id = models.CharField(max_length=20)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    target = models.CharField(max_length=30, choices=TARGET_CHOICES)
    amount = models.FloatField(null=True, blank=True)

    def get_json(self):
        return dict(
            rz_order_id=self.rz_order_id,
            user=self.user.id,
            target=self.target,
            target_str=self.get_target_display(),
            amount=self.amount
        )


class Transaction(DateMixing):
    order_info = models.ForeignKey(Order, on_delete=models.CASCADE)
    amount = models.FloatField(null=True, blank=True)
    txn_id = models.CharField(max_length=40, null=True, blank=True)
    currency = models.CharField(max_length=10, default="INR")
    method = models.CharField(max_length=20, null=True, blank=True)
    card_id = models.CharField(max_length=20, null=True, blank=True)
    bank = models.CharField(max_length=20, null=True, blank=True)
    wallet = models.CharField(max_length=20, null=True, blank=True)
    vpa = models.CharField(max_length=40, null=True, blank=True)
    status = models.CharField(max_length=20, null=True, blank=True)

    def get_json(self):
        return dict(
            order=self.order_info.id,
            amount=self.amount,
            txn_id=self.txn_id,
            currency=self.currency,
            method=self.method,
            card_id=self.card_id,
            bank=self.bank,
            wallet=self.wallet,
            vpa=self.vpa,
            status=self.status)

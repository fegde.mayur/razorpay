import json
from django.http import JsonResponse
from pg_default.settings import client
from django.contrib.auth.models import User
from payment.models import Transaction, Order
from payment.compose import update_or_create_txn
from django.views.decorators.http import require_http_methods
# Create your views here.


@require_http_methods(['POST'])
def order(request):
    params = json.loads(request.body)
    data_dict = {"amount": params['amount'], "currency": "INR"}
    response = client.order.create(data=data_dict)
    params['user'] = User.objects.get(id=params['user'])
    params['rz_order_id'] = response['id']
    instance = Order.objects.create(**params)
    return JsonResponse(dict(validation="Success", status=True, data=instance.get_json()), status=200)


@require_http_methods(['POST'])
def process_transaction(request):
    params = json.loads(request.body)
    payment_id = params.get('payment_id')
    txn_response = client.payment.fetch(payment_id)
    update_or_create_txn(txn_response)
    return JsonResponse(dict(validation="Success", status=True), status=200)


@require_http_methods(['GET'])
def transaction(request, pk=None):
    response = []
    kwargs = {}
    if pk:
        kwargs['id'] = pk
    queryset = Transaction.objects.filter(**kwargs)
    for instance in queryset:
        response.append(instance.get_json())
    return JsonResponse(dict(validation="Success", status=True, data=response), status=200)


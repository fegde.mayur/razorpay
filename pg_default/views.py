import json
from django.http import JsonResponse
from pg_default.models import Expert
from django.contrib.auth.models import User


def expert(request, pk=None):
    response = []
    if request.method == "POST":
        params = json.loads(request.body)
        user_instance: User = User.objects.get(id=1)
        params['user'] = user_instance
        instance = Expert.objects.create(**params)
        return JsonResponse(dict(validation="Created", status=True,
                                 data=[instance.get_json()]), status=200)

    if request.method == "GET":
        kwargs = {}
        if pk:
            kwargs['id'] = pk
        queryset = Expert.objects.filter(**kwargs)
        for instance in queryset:
            response.append(instance.get_json())
        return JsonResponse(dict(validation="", status=True, data=response), status=200)

from django.db import models
from django.contrib.auth.models import User


class DateMixing(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)


class Expert(DateMixing):
    PROFESSION_CHOICES = (('1', 'Student'), ('2', 'Broker'),
                          ('3', 'Analyst'), ('4', 'Financial Advisor'))

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    profession = models.CharField(max_length=40, choices=PROFESSION_CHOICES)
    mobile = models.CharField(max_length=10, null=True, blank=True)

    def get_json(self):
        return dict(
            user=self.user.id,
            profession=self.profession,
            profession_str=self.get_profession_display(),
            mobile=self.mobile)